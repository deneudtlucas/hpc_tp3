#include <thread>
#include <mutex>
#include "Camera.hpp"
#include "Rayon.hpp"

std::mutex mtx;
static int zone_index_x = 0;
static int zone_index_y = 0;

Camera::Camera() {
    position = Point(0.0, 0.0, 2.0);;
    cible = Point(0.0, 0.0, 0.0);
    distance = 2.0;
}

Camera::~Camera() {}

void Camera::genererImage(const Scene &sc, Image &im, int profondeur) {

    // Calcul des dimensions d'un pixel par rapport
    // à la résolution de l'image - Les pixels doivent être carrés
    // pour éviter les déformations de l'image.
    // On fixe :
    // - les dimensions en largeur de l'écran seront comprises dans [-1, 1]
    // - les dimensions en hauteur de l'écran soront comprises dans [-H/L, H/L]
    float cotePixel = 2.0 / im.getLargeur();

    // Pour chaque pixel
    for (int i = 0; i < im.getLargeur(); i++) {
        for (int j = 0; j < im.getHauteur(); j++) {

            // calcul des coordonnées du centre du pixel
            float milieuX = -1 + (i + 0.5f) * cotePixel;
            float milieuY = (float) im.getHauteur() / (float) im.getLargeur()
                            - (j + 0.5f) * cotePixel;

            Point centre(milieuX, milieuY, 0);

            // Création du rayon
            Vecteur dir(position, centre);
            dir.normaliser();
            Rayon ray(position, dir);

            // Lancer du rayon primaire
            Intersection inter;
            if (sc.intersecte(ray, inter)) {
                im.setPixel(i, j, inter.getCouleur(sc, position, profondeur));
            } else
                im.setPixel(i, j, sc.getFond());

        }// for j

    }// for i
}


void Camera::genererImageParallele(const Scene &sc, Image &im, int profondeur, int nbThreads) {
    std::thread threads[nbThreads];

    for (int i = 0; i < nbThreads; i++) {
        threads[i] = std::thread(calculerZone, std::ref(sc), std::ref(im), profondeur, position);
    }

    for (std::thread &thread : threads) {
        thread.join();
    }
}

void Camera::calculerZone(const Scene &sc, Image &im, int profondeur, const Point &position) {
    clock_t start = clock();
    bool end = false;
    zone area = ZoneSuivante(std::ref(im));
    int compteur = 0;
    float cotePixel = 2.0 / im.getLargeur();
    while (!end) {
        // Pour chaque pixel
        for (int i = area.x; i < area.x + area.largeur; i++) {
            for (int j = area.y; j < area.y + area.hauteur; j++) {

                // calcul des coordonnées du centre du pixel
                float milieuX = -1 + (i + 0.5f) * cotePixel;
                float milieuY = (float) im.getHauteur() / (float) im.getLargeur()
                                - (j + 0.5f) * cotePixel;

                Point centre(milieuX, milieuY, 0);

                // Création du rayon
                Vecteur dir(position, centre);
                dir.normaliser();
                Rayon ray(position, dir);

                // Lancer du rayon primaire
                Intersection inter;
                if (sc.intersecte(ray, inter)) {
                    im.setPixel(i, j, inter.getCouleur(sc, position, profondeur));
                } else {
                    im.setPixel(i, j, sc.getFond());
                }
            }
        }
        compteur++;
        area = ZoneSuivante(std::ref(im));
        if (area.x == -1 or area.y == -1) {
            end = true;
        }
    }
    std::thread::id this_id = std::this_thread::get_id();
    float time = ((clock() - start ) / (double) CLOCKS_PER_SEC) / 10;
    mtx.lock();
    std::cout << "thread id: " << this_id << ", zone couvertes:" << compteur << ", time spent: " << time << "s" << std::endl;
    mtx.unlock();
}

zone Camera::ZoneSuivante(const Image &im) {
    zone area{};
    int larg = im.getLargeur() / LARGZONE;
    int haut = im.getHauteur() / HAUTZONE;

    if (larg == zone_index_x and haut == zone_index_y) {
        area.x = -1;
        area.y = -1;
        return area;
    }
    area.x = zone_index_x * LARGZONE;
    area.y = zone_index_y * HAUTZONE;
    if (larg == zone_index_x) {
        area.largeur = im.getLargeur() - area.x;
        zone_index_x = 0;
        zone_index_y++;
    } else {
        area.largeur = LARGZONE;
        zone_index_x++;
    }
    if (haut == zone_index_y) {
        area.hauteur = im.getHauteur() - area.y;
    } else {
        area.hauteur = HAUTZONE;
    }
    return area;
}


ostream &operator<<(ostream &out, const Camera &c) {

    out << " position = " << c.position << " - cible = " << c.cible;
    out << " - distance = " << c.distance << flush;
    return out;
}
